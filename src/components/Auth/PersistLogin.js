import { Fragment, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import useLogout from "../../hooks/useLogout";
import useRefreshToken from "../../hooks/useRefreshToken";

const PersistLogin = (props) => {
  const [isLoading, setIsLoading] = useState(true);
  const getRefreshToken = useRefreshToken();
  const { auth } = useSelector((state) => state.auth);
  const logout = useLogout();

  useEffect(() => {
    const verifyRefreshToken = async () => {
      try {
        await getRefreshToken();
      } catch (err) {
        console.log(err);
        await logout();
      } finally {
        setIsLoading(false);
      }
    };

    !auth?.accessToken ? verifyRefreshToken() : setIsLoading(false);
  }, []);

  // useEffect(() => {
  //   console.log(`isLoading: ${isLoading}`);
  //   console.log(`auth token: ${JSON.stringify(auth?.accessToken)}`);
  // }, [isLoading]);

  return <Fragment>{isLoading ? <p>Loading...</p> : props.children}</Fragment>;
};

export default PersistLogin;
