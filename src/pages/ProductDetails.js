import { useEffect, useState } from "react";
import { useParams } from "react-router";
import Product from "../components/Products/Product";

import { getProduct } from "../services/product.service";

const ProductDetails = () => {
  const params = useParams();
  const [product, setProduct] = useState({});

  useEffect(() => {
    if (params.name) {
      const product_name = params.name.replace(/-/g, " ");
      const fetchProduct = async () => {
        try {
          const response = await getProduct(product_name);
          // console.log(response)
          setProduct(response.data);
        } catch (err) {
          console.log(err);
        }
      };
      fetchProduct();
    }
  }, [params.name]);

  return (
    <div>
      <Product product={product} />
    </div>
  );
};

export default ProductDetails;
