import { Link } from "react-router-dom";
import classes from "./ProductItem.module.css";

const ProductItem = (props) => {
  const { id, name, price, images } = props.product;

  const formatted_name = name.replace(/\s+/g, '-').toLowerCase();
  return (
    <Link to={`/product/${formatted_name}`} key={id} className={classes.product}>
      <img src={images[0]} alt={name} />
      <div className={classes["product-name"]}>{name}</div>
      <div className={classes["product-price"]}>${price.toFixed(2)}</div>
    </Link>
  );
};

export default ProductItem;
