import CartProductList from "../components/Cart/CartProductList";
import CartTotal from "../components/Cart/CartTotal";
import Card from "../components/UI/Card";
import classes from "./CartPage.module.css";

const CartPage = () => {
  return (
    <div className={classes.cart}>
      <div className={classes.header}>Your Cart</div>

      <div className="row">
        <Card className={classes["product-section"]}>
          <div className={classes.title}>Items</div>
          <div className="divider"></div>
          <CartProductList />
        </Card>

        <Card className={classes["total-section"]}>
          <div className={classes.title}>Total</div>
          <div className="divider"></div>
          <CartTotal />
        </Card>
      </div>
    </div>
  );
};

export default CartPage;
