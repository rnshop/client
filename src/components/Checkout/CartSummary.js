import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import CartProductList from "../Cart/CartProductList";
import Card from "../UI/Card";

import classes from "./CartSummary.module.css";

const CartSummary = () => {
  const history = useHistory();
  const items = useSelector((state) => state.cart.items);

  const editCartHandler = () => {
    history.push("/cart");
  };

  return (
    <Card className={classes.cart}>
      <div className={classes.top}>
        <div>YOUR CART</div>
        <div onClick={editCartHandler} className={classes.edit}>
          Edit Cart
        </div>
      </div>

      <div className="divider"></div>

      <CartProductList items={items} />
    </Card>
  );
};

export default CartSummary;
