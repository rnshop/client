import { Fragment, useRef, useState } from "react";
import { useSelector } from "react-redux";

import Input from "../UI/Input";
import Button from "../UI/Button";
import classes from "./CartTotal.module.css";
import { useHistory } from "react-router";

const CartTotal = () => {
  const { totalAmount } = useSelector((state) => state.cart);
  const [openPromo, setOpenPromo] = useState(false);
  const promoCodeRef = useRef();
  const history = useHistory();

  const togglePromoHandler = () => {
    if (!openPromo) {
      setOpenPromo(true);
    } else {
      setOpenPromo(false);
    }
  };

  const promoCodeInput = {
    id: "promoCode",
    type: "text",
  };

  const applyPromoCodeHandler = () => {
    console.log(promoCodeRef.current.value);
  };

  const checkoutHandler = () => {
    history.push("/checkout");
  };

  const promoCodeClasses = `${classes["promocode-section"]} 
  ${openPromo ? classes.overwrite : ""}`;

  return (
    <Fragment>
      <div className={classes.total}>
        <span>Total:</span>
        <span>${totalAmount.toFixed(2)}</span>
      </div>

      <div className="divider"></div>

      <div className={promoCodeClasses} onClick={togglePromoHandler}>
        <div>Have a Promo Code?</div>

        {!openPromo && (
          <i className={`fa fa-plus`}>
            <span></span>
          </i>
        )}
        {openPromo && (
          <i className={`fa fa-minus`}>
            <span></span>
          </i>
        )}
      </div>

      {openPromo && (
        <div>
          <Input
            classes={classes["promocode-input"]}
            ref={promoCodeRef}
            input={promoCodeInput}
            label="Promo Code"
          />
          <Button className={classes.apply} onClick={applyPromoCodeHandler}>
            Apply
          </Button>
        </div>
      )}
      <div className="divider"></div>

      <Button className={classes.checkout} onClick={checkoutHandler}>
        Checkout
      </Button>
    </Fragment>
  );
};

export default CartTotal;
