import http from "../http";
import { useDispatch } from "react-redux";
import { authActions } from "../store/auth-slice";

const useLogout = () => {
  const dispatch = useDispatch();

  const logout = async () => {
    dispatch(authActions.setAuth({}));
    try {
      await http("/logout");
    } catch (err) {
      console.error(err);
    }
  };

  return logout;
};

export default useLogout;
