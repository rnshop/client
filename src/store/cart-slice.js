import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  items: [],
  totalAmount: 0,
};

if (localStorage.getItem("cart")) {
  const cart = JSON.parse(localStorage.getItem("cart"));
  initialState.items = cart.items;
  initialState.totalAmount = cart.totalAmount;
} else {
  initialState.items = [];
  initialState.totalAmount = 0;
}

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addItem(state, action) {
      // console.log("add item")
      const { _id, price, quantity } = action.payload;

      state.totalAmount = state.totalAmount + price * quantity;

      const matchedIndex = state.items.findIndex((item) => item._id === _id);
      if (matchedIndex > -1) {
        state.items[matchedIndex].quantity =
          state.items[matchedIndex].quantity + quantity;
      } else {
        state.items.push(action.payload);
      }
    },

    removeItem(state, action) {
      const itemId = action.payload;

      const matchedItem = state.items.find((item) => item._id === itemId);
      if (matchedItem) {
        state.items = state.items.filter((item) => item._id !== itemId);

        state.totalAmount =
          state.totalAmount - matchedItem.price * matchedItem.quantity;
      }
    },

    increase(state, action) {
      // console.log('increase action ', action.payload)
      const { _id, price, diff } = action.payload;

      state.totalAmount = state.totalAmount + price * diff;

      const matchedIndex = state.items.findIndex((item) => item._id === _id);
      if (matchedIndex > -1) {
        state.items[matchedIndex].quantity =
          state.items[matchedIndex].quantity + diff;
      }
    },

    decrease(state, action) {
      const { _id, price, diff } = action.payload;

      state.totalAmount = state.totalAmount - price * diff;

      const matchedIndex = state.items.findIndex((item) => item._id === _id);
      if (matchedIndex > -1) {
        if (state.items[matchedIndex].quantity === 1) {
          state.items = state.items.filter((item) => item._id !== _id);
        } else {
          state.items[matchedIndex].quantity =
            state.items[matchedIndex].quantity - diff;
        }
      }
    },

    clear(state) {
      state = initialState;
    },
  },
});

export const cartActions = cartSlice.actions;

export default cartSlice.reducer;
