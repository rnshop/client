import { Fragment } from "react";

import Customer from "../components/Checkout/Customer";
import Delivery from "../components/Checkout/Delivery";
import CartSummary from "../components/Checkout/CartSummary";

import classes from "./Checkout.module.css";

const Checkout = () => {
  return (
    <Fragment>
      <h2>Secure Checkout</h2>

      <div className={classes["checkout-wrapper"]}>
        <div className={classes.info}>
          <Customer />
          <Delivery />
        </div>

        <CartSummary />
      </div>
    </Fragment>
  );
};

export default Checkout;
