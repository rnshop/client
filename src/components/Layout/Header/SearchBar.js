import { Fragment } from "react";
import classes from "./SearchBar.module.css";

const SearchBar = () => {
  return (
    <Fragment>
      <div className={classes["input-icons"]}>
        <i className={`fa fa-search`}></i>
        <input
          className={classes["input-field"]}
          type="text"
          placeholder="Search"
        />
      </div>
    </Fragment>
  );
};

export default SearchBar;
