import { useEffect, useState } from "react";
import classes from "./ProductQtyControl.module.css";

const ProductQtyControl = (props) => {
  const [quantity, setQuantity] = useState(+props.item.quantity);
  const item = props.item;

  useEffect(() => {
    setQuantity(item.quantity);
  }, [item]);

  const decreaseHandler = (itemId) => {
    props.onDecreaseQty(itemId);
  };

  const increaseHandler = (itemId) => {
    props.onIncreaseQty(itemId);
  };

  const updateQuantityHandler = (updatedQty) => {
    setQuantity(+updatedQty);
    props.onUpdateQty(+updatedQty);
  };

  const quantityBlurHandler = (itemId) => {
    props.onQuantityBlur(itemId);
  };

  return (
    <div className={classes["quantity-container"]}>
      <div>{props.text}:</div>
      <div className={classes.control}>
        <span
          className={classes.decrease}
          onClick={() => decreaseHandler(item._id)}
        >
          <i className={`fa-solid fa-minus`}></i>
        </span>
        <input
          value={quantity}
          onChange={(e) => updateQuantityHandler(e.target.value)}
          onBlur={() => quantityBlurHandler(item._id)}
        />
        <span
          className={classes.increase}
          onClick={() => increaseHandler(item._id)}
        >
          <i className={`fa-solid fa-add`}></i>
        </span>
      </div>
    </div>
  );
};

export default ProductQtyControl;
