import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { productActions } from "../store/product-slice";
import { getAllProducts } from "../services/product.service";

import ProductList from "../components/Products/ProductList";


const AllProducts = () => {
  const products = useSelector((state) => state.product.products);
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const response = await getAllProducts();
        dispatch(productActions.setProducts(response.data));
      } catch (err) {
        if (err.response) {
          console.log(err.response);
        } else {
          console.log(`Error: ${err.message}`);
        }
      }
    };

    fetchProducts();
  }, [dispatch]);

  return (
    <div>
      <ProductList products={products} />
    </div>
  );
};

export default AllProducts;
