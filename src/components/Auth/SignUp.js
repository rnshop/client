import useInput from "../../hooks/useInput";
import { useState } from "react";

import Input from "../UI/Input";
import Button from "../UI/Button";

import { registerUser } from "../../services/user.service";

const SignUp = (props) => {
  const isNotEmpty = (value) => value.trim() !== "";

  const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const isValidEmail = (value) => value.match(emailRegex);

  // const passwordRegex = new RegExp(
  //   "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})"
  // );
  // const isPasswordValid = (value) => passwordRegex.test(value);

  const [confirmPwdError, setConfirmPwdError] = useState(null);

  const firstNameProp = {
    id: "firstName",
    type: "text",
    required: true,
  };

  const lastNameProp = {
    id: "lastName",
    type: "text",
    required: true,
  };

  const emailProp = {
    id: "email",
    type: "email",
    required: true,
  };

  const passwordProp = {
    id: "password",
    type: "password",
    required: true,
  };

  const confirmPasswordProp = {
    id: "confirmPassword",
    type: "password",
    required: true,
  };

  const {
    value: email,
    isValid: emailIsValid,
    hasError: emailHasError,
    inputChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
  } = useInput(isValidEmail);

  const {
    value: firstName,
    isValid: firstNameIsValid,
    hasError: firstNameHasError,
    inputChangeHandler: firstNameChangeHandler,
    inputBlurHandler: firstNameBlurHandler,
  } = useInput(isNotEmpty);

  const {
    value: lastName,
    isValid: lastNameIsValid,
    hasError: lastNameHasError,
    inputChangeHandler: lastNameChangeHandler,
    inputBlurHandler: lastNameBlurHandler,
  } = useInput(isNotEmpty);

  const {
    value: password,
    isValid: passwordIsValid,
    hasError: passwordHasError,
    inputChangeHandler: passwordChangeHandler,
    inputBlurHandler: passwordBlurHandler,
  } = useInput(isNotEmpty);

  const {
    value: confirmPassword,
    isValid: confirmPasswordIsValid,
    hasError: confirmPasswordHasError,
    inputChangeHandler: confirmPasswordChangeHandler,
    inputBlurHandler: confirmPasswordBlurHandler,
  } = useInput(isNotEmpty);

  let formIsValid = false;
  if (
    firstNameIsValid &&
    lastNameIsValid &&
    emailIsValid &&
    passwordIsValid &&
    confirmPasswordIsValid
  ) {
    formIsValid = true;
  }

  const submitHandler = async (e) => {
    e.preventDefault();

    // console.log("aa");
    // console.log(password);
    // console.log(confirmPassword);

    if (password === confirmPassword) {
      setConfirmPwdError(null);
      formIsValid = true;

      // save user
      const user = {
        email,
        firstName,
        lastName,
        password,
      };

      try {
        const response = await registerUser(user);
        console.log("user created ", response);
        props.onCloseModal();
      } catch (err) {
        console.log(err.response.data.message);
      }
    } else {
      formIsValid = false;
      setConfirmPwdError(
        "Confirm password is not matched with the entered password"
      );
    }
  };

  return (
    <form onSubmit={submitHandler}>
      <Input
        label="Email"
        classes="input-full"
        input={emailProp}
        value={email}
        onChange={emailChangeHandler}
        onBlur={emailBlurHandler}
      />
      {emailHasError && (
        <p className="error-text">Please fill in a valid email.</p>
      )}

      <Input
        label="First Name"
        classes="input-full"
        input={firstNameProp}
        value={firstName}
        onChange={firstNameChangeHandler}
        onBlur={firstNameBlurHandler}
      />
      {firstNameHasError && (
        <p className="error-text">Please fill in first name.</p>
      )}

      <Input
        label="Last Name"
        classes="input-full"
        input={lastNameProp}
        value={lastName}
        onChange={lastNameChangeHandler}
        onBlur={lastNameBlurHandler}
      />
      {lastNameHasError && (
        <p className="error-text">Please fill in last name.</p>
      )}

      <Input
        label="Password"
        classes="input-full"
        input={passwordProp}
        value={password}
        onChange={passwordChangeHandler}
        onBlur={passwordBlurHandler}
      />
      {passwordHasError && <p className="error-text">Password is required</p>}

      <Input
        label="Confirm Password"
        classes="input-full"
        input={confirmPasswordProp}
        value={confirmPassword}
        onChange={confirmPasswordChangeHandler}
        onBlur={confirmPasswordBlurHandler}
      />
      {confirmPasswordHasError && (
        <div className="error-text">Confirm password is required</div>
      )}

      {confirmPwdError && !confirmPasswordHasError && (
        <div className="error-text">{confirmPwdError}</div>
      )}

      <Button disabled={!formIsValid}>Create Account</Button>
    </form>
  );
};

export default SignUp;
