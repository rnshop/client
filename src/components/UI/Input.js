import { forwardRef } from "react";
import classes from "./Input.module.css";

const Input = forwardRef((props, ref) => {
  return (
    <div className={`${props.classes} ${classes.input}`}>
      <div className={classes["label-wrapper"]}>
        <label htmlFor={props.input.id}>{props.label}</label>
        {props.input.required && <span>*</span>}
      </div>
      <input
        ref={ref}
        id={props.input.id}
        {...props.input}
        value={props.value}
        onChange={props.onChange}
        onBlur={props.onBlur}
      />
    </div>
  );
});

export default Input;
