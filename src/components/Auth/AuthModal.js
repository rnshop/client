import Modal from "../UI/Modal";
import classes from "./AuthModal.module.css";

const AuthModal = (props) => {
  return (
    <Modal classes={classes['auth-modal']} onClose={props.onClose}>
      {props.children}
    </Modal>
  );
};

export default AuthModal;
