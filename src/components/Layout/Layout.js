import React from "react";
import { Fragment } from "react";
import { useSelector } from "react-redux";

import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import Cart from "../Cart/Cart";
import Auth from '../Auth/Auth'

import classes from "./Layout.module.css";



const Layout = (props) => {
  const { isCartModalOpen, isLoginModalOpen } = useSelector((state) => state.ui);

  return (
    <Fragment>
      {isCartModalOpen && <Cart />}
      {isLoginModalOpen && <Auth />}
      <Header />
      <div className={classes.content}>{props.children}</div>
      {/* <Footer /> */}
    </Fragment>
  );
};

export default Layout;
