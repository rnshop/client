import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  auth: {
    email: "",
    firstName: "",
    accessToken: "",
  },
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setAuth(state, action) {
      const { email, firstName, accessToken } = action.payload;
      state.auth.email = email;
      state.auth.firstName = firstName;
      state.auth.accessToken = accessToken;
    },
  },
});

export const authActions = authSlice.actions;

export default authSlice.reducer;
