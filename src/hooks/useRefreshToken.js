import { useDispatch } from "react-redux";
import http from "../http";
import { authActions } from "../store/auth-slice";
import useLogout from "./useLogout";

const useRefreshToken = () => {
  const dispatch = useDispatch();
  const logout = useLogout();

  const refreshToken = async () => {
    try {
      const response = await http.get("/refresh");

      // console.log("refresh token ", response.data)
      dispatch(
        authActions.setAuth({
          email: response.data.email,
          firstName: response.data.firstName,
          accessToken: response.data.accessToken,
        })
      );

      return response.data.accessToken;
    } catch (err) {
      await logout();
    }
  };

  return refreshToken;
};

export default useRefreshToken;
