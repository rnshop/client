import { useHistory } from "react-router-dom";
import { useState } from "react";
import useInput from "../../hooks/useInput";
import Input from "../../components/UI/Input";
import { saveProduct } from "../../services/product.service";

import classes from "./ProductForm.module.css";

const ProductForm = () => {
  const [selectedFile, setSelectedFile] = useState(null);
  const history = useHistory();

  const isNotEmpty = (value) => value.trim() !== "";

  const {
    value: name,
    isValid: nameIsValid,
    hasError: nameHasError,
    inputChangeHandler: nameChangeHandler,
    inputBlurHandler: nameBlurHandler,
  } = useInput(isNotEmpty);

  const {
    value: price,
    isValid: priceIsValid,
    hasError: priceHasError,
    inputChangeHandler: priceChangeHandler,
    inputBlurHandler: priceBlurHandler,
  } = useInput(isNotEmpty);

  const {
    value: stock,
    isValid: stockIsValid,
    hasError: stockHasError,
    inputChangeHandler: stockChangeHandler,
    inputBlurHandler: stockBlurHandler,
  } = useInput(isNotEmpty);

  const {
    value: desc,
    isValid: descIsValid,
    hasError: descHasError,
    inputChangeHandler: descChangeHandler,
    inputBlurHandler: descBlurHandler,
  } = useInput(isNotEmpty);

  //   validate form
  let formIsValid = false;
  if (nameIsValid && priceIsValid && stockIsValid && descIsValid) {
    formIsValid = true;
  }

  const fileSelectedHandler = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  const submitFormHandler = async (e) => {
    e.preventDefault();

    if (!formIsValid) {
      return;
    }

    // post data to api
    // back to listing page

    const formData = new FormData();
    formData.append("name", name);
    formData.append("price", price);
    formData.append("noOfStock", stock);
    formData.append("description", desc);
    formData.append("image", selectedFile);

    try {
      const response = await saveProduct(formData);
      console.log("product created ", response.data);
      history.push("/");
    } catch (err) {
      console.log(err.message);
      if (err.response?.data) {
        console.log(err.response.data);
      }
    }
  };

  const nameProp = {
    id: "name",
    type: "text",
    required: true,
  };

  const priceProp = {
    id: "price",
    type: "number",
    required: true,
  };

  const stockProp = {
    id: "stock",
    type: "number",
    required: true,
  };

  return (
    <form onSubmit={submitFormHandler}>
      <Input
        label="Name"
        classes="input-half"
        input={nameProp}
        value={name}
        onChange={nameChangeHandler}
        onBlur={nameBlurHandler}
      />
      {nameHasError && (
        <p className="error-text">Please fill in product name.</p>
      )}

      <Input
        label="Price"
        classes="input-half"
        input={priceProp}
        value={price}
        onChange={priceChangeHandler}
        onBlur={priceBlurHandler}
      />
      {priceHasError && (
        <p className="error-text">Please fill in product price.</p>
      )}

      <Input
        label="No of Stock"
        classes="input-half"
        input={stockProp}
        value={stock}
        onChange={stockChangeHandler}
        onBlur={stockBlurHandler}
      />
      {stockHasError && (
        <p className="error-text">Please fill in no of stock.</p>
      )}

      <div style={{marginBottom: '1rem'}}>
        <div className={classes["label-wrapper"]}>
          <label htmlFor="desc" id="desc">
            Description
          </label>
          <span>*</span>
        </div>

        <textarea
          id="desc"
          name="desc"
          rows="10"
          value={desc}
          onChange={descChangeHandler}
          onBlur={descBlurHandler}
        />
      </div>

      {descHasError && (
        <p className="error-text">Please fill in description.</p>
      )}

      <br />

      <input id="files" type="file" onChange={fileSelectedHandler} />

      <div className="form-actions">
        <button disabled={!formIsValid}>Submit</button>
      </div>
    </form>
  );
};

export default ProductForm;
