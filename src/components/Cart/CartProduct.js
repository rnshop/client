import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { cartActions } from "../../store/cart-slice";
import { uiActions } from "../../store/ui-slice";
import ProductQtyControl from "../Products/ProductQtyControl";
import classes from "./CartProduct.module.css";

const CartProduct = (props) => {
  const item = props.item;

  const history = useHistory();
  const [updatedQty, setUpdatedQty] = useState();
  const dispatch = useDispatch();
  const { items } = useSelector((state) => state.cart);

  const decreaseHandler = (itemId) => {
    if (localStorage.getItem("cart")) {
      const cart = JSON.parse(localStorage.getItem("cart"));

      const cartItems = cart.items;
      const cartTotal = cart.totalAmount;

      const matchedIndex = cartItems.findIndex((item) => item._id === itemId);
      if (matchedIndex > -1) {
        let updatedCart = { ...cart };
        let updatedCartItems = [...updatedCart.items];

        let updatedTotal = cartTotal - updatedCartItems[matchedIndex].price;
        updatedCart.totalAmount = +updatedTotal.toFixed(2);

        if (updatedCartItems[matchedIndex].quantity === 1) {
          updatedCartItems = updatedCartItems.filter((i) => i._id !== itemId);
        } else {
          updatedCartItems[matchedIndex].quantity =
            updatedCartItems[matchedIndex].quantity - 1;
        }
        updatedCart.items = updatedCartItems;

        localStorage.setItem("cart", JSON.stringify(updatedCart));

        dispatch(
          cartActions.decrease({
            _id: cartItems[matchedIndex]._id,
            price: cartItems[matchedIndex].price,
            diff: 1,
          })
        );
      }
    }
  };

  const increaseHandler = (itemId) => {
    if (localStorage.getItem("cart")) {
      const cart = JSON.parse(localStorage.getItem("cart"));

      const cartItems = cart.items;
      const cartTotal = cart.totalAmount;

      const matchedIndex = cartItems.findIndex((item) => item._id === itemId);
      if (matchedIndex > -1) {
        let updatedCart = { ...cart };
        let updatedCartItems = [...updatedCart.items];
        updatedCartItems[matchedIndex].quantity =
          updatedCartItems[matchedIndex].quantity + 1;
        updatedCart.items = updatedCartItems;

        let updatedTotal = cartTotal + updatedCartItems[matchedIndex].price;
        updatedCart.totalAmount = +updatedTotal.toFixed(2);

        localStorage.setItem("cart", JSON.stringify(updatedCart));

        dispatch(
          cartActions.increase({
            _id: cartItems[matchedIndex]._id,
            price: cartItems[matchedIndex].price,
            diff: 1,
          })
        );
      }
    }
  };

  const updateQuantityHandler = (updatedQty) => {
    setUpdatedQty(updatedQty);
  };

  const quantityBlurHandler = (itemId) => {
    const matchedItem = items.find((item) => item._id === itemId);
    if (matchedItem) {
      const currrentQty = matchedItem.quantity;
      if (currrentQty !== updatedQty) {
        if (currrentQty > updatedQty) {
          dispatch(
            cartActions.decrease({
              _id: matchedItem._id,
              price: matchedItem.price,
              diff: currrentQty - updatedQty,
            })
          );
        } else {
          dispatch(
            cartActions.increase({
              _id: matchedItem._id,
              price: matchedItem.price,
              diff: updatedQty - currrentQty,
            })
          );
        }
      }
    }
  };

  const removeItemHandler = (itemId) => {
    if (localStorage.getItem("cart")) {
      const cart = JSON.parse(localStorage.getItem("cart"));
      const cartItems = cart.items;
      const cartTotal = cart.totalAmount;

      const matchedItem = cartItems.find((item) => item._id === itemId);
      if (matchedItem) {
        // console.log("removing");
        let updatedCart = { ...cart };
        let updatedCartItems = [...updatedCart.items];
        updatedCartItems = updatedCartItems.filter((i) => i._id !== itemId);
        updatedCart.items = updatedCartItems;

        let updatedTotal = cartTotal - matchedItem.price * matchedItem.quantity;
        updatedCart.totalAmount = +updatedTotal.toFixed(2);

        localStorage.setItem("cart", JSON.stringify(updatedCart));

        dispatch(cartActions.removeItem(itemId));
      }
    }
  };

  const productLinkHandler = () => {
    const formatted_name = item.name.replace(/\s+/g, "-").toLowerCase();
    history.push(`/product/${formatted_name}`);
    closeCartModal();
  };

  const closeCartModal = () => {
    dispatch(uiActions.closeCartModal());
  };

  return (
    <div key={item._id} className={classes["product-wrapper"]}>
      <div className={classes.product}>
        {item.image && (
          <div className={classes.image}>
            <img src={item.image} />
          </div>
        )}

        <div className={classes.details}>
          <div onClick={productLinkHandler} className={classes["product-name"]}>
            {item.name}
          </div>
          <div>${item.price.toFixed(2)}</div>

          <ProductQtyControl
            item={item}
            text="Qty"
            onIncreaseQty={increaseHandler}
            onDecreaseQty={decreaseHandler}
            onUpdateQty={updateQuantityHandler}
            onQuantityBlur={quantityBlurHandler}
          />
        </div>
      </div>

      <div onClick={() => removeItemHandler(item._id)}>
        <i className={`fa-solid fa-times ${classes.remove}`}></i>
      </div>
    </div>
  );
};

export default CartProduct;
