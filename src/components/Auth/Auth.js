import { Fragment, useState } from "react";
import { useDispatch } from "react-redux";
import { uiActions } from "../../store/ui-slice";

import AuthModal from "./AuthModal";
import Login from "./Login";
import SignUp from "./SignUp";

import classes from "./Auth.module.css";

const Auth = () => {
  const dispatch = useDispatch();
  const [signup, setSignup] = useState(false);

  const closeLoginModal = () => {
    dispatch(uiActions.closeLoginModal());
  };

  const signUpHandler = () => {
    setSignup(true);
  };

  const loginHandler = () => {
    setSignup(false);
  };
  return (
    <Fragment>
      <AuthModal onClose={closeLoginModal}>
        <div className={classes.header}>{!signup ? "Login" : "Sign Up"}</div>

        {!signup && (
          <Fragment>
            <Login onCloseModal={closeLoginModal} />
            <div className={classes["create-account"]}>
              <div>Don't have an account?</div>
              <div className={classes.create} onClick={signUpHandler}>
                Create Account
              </div>
            </div>
          </Fragment>
        )}
        {signup && (
          <Fragment>
            <SignUp onCloseModal={closeLoginModal} />
            <div className={classes["create-account"]}>
              <div>Already have an account?</div>
              <div className={classes.create} onClick={loginHandler}>
                Login
              </div>
            </div>
          </Fragment>
        )}
      </AuthModal>
    </Fragment>
  );
};

export default Auth;
