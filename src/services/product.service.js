import http from "../http";

export const getAllProducts = () => {
  try {
    const response = http.get("/products");
    return response;
  } catch (err) {
    return err;
  }
};

export const getProduct = (productName) => {
  try {
    const response = http.get(`/products/${productName}`);
    return response;
  } catch (err) {
    return err;
  }
};

export const saveProduct = (formData) => {
  try {
    const response = http.post("/products", formData);
    return response;
  } catch (err) {
    return err;
  }
};
