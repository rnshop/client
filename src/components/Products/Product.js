import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import classes from "./Product.module.css";
import Button from "../UI/Button";
import { cartActions } from "../../store/cart-slice";
import { uiActions } from "../../store/ui-slice";

const Product = (props) => {
  const { _id, name, price, description, images } = props.product;

  const dispatch = useDispatch();
  const [quantity, setQuantity] = useState(1);
  const [thumbnailUrl, setThumbnailUrl] = useState("");

  useEffect(() => {
    setQuantity(1);
    if (images) setThumbnailUrl(images[0]);
  }, [props.product]);

  const addToCart = () => {
    // console.log("add to cart");
    const cart = localStorage.getItem("cart")
      ? JSON.parse(localStorage.getItem("cart"))
      : {};

    const productToAdd = {
      _id,
      name,
      price: +price,
      image: images[0],
      quantity,
    };

    const cartIsEmpty = Object.keys(cart).length === 0;
    if (!cartIsEmpty) {
      const cartItems = cart.items;
      const cartTotal = cart.totalAmount;
      const matchedIndex = cartItems.findIndex((item) => item._id === _id);
      let updatedTotal = cartTotal + price * quantity;

      let updatedCart = { ...cart };
      let updatedCartItems = [...updatedCart.items];
      if (matchedIndex > -1) {
        updatedCartItems[matchedIndex].quantity =
          updatedCartItems[matchedIndex].quantity + quantity;
      } else {
        updatedCartItems.push(productToAdd);
      }
      updatedCart.items = updatedCartItems;
      updatedCart.totalAmount = +updatedTotal.toFixed(2);
      localStorage.setItem("cart", JSON.stringify(updatedCart));
      
    } else {
      const products = [];
      products.push(productToAdd);

      cart.items = products;
      cart.totalAmount = price * quantity;
      localStorage.setItem("cart", JSON.stringify(cart));
    }

    dispatch(cartActions.addItem(productToAdd));
    dispatch(uiActions.openCartModal());
  };

  const increaseHandler = () => {
    setQuantity((prevQty) => {
      return prevQty + 1;
    });
  };

  const decreaseHandler = () => {
    setQuantity((prevQty) => {
      return prevQty - 1;
    });
  };

  const updateQuantityHandler = (e) => {
    const quantity = e.target.value;
    setQuantity(+quantity);
  };

  const thumbnailSelectedHandler = (url) => {
    setThumbnailUrl(url);
  };

  const swipeLeftHandler = () => {
    const matchedIdx = images.findIndex((url) => url === thumbnailUrl);
    console.log(matchedIdx);

    if (matchedIdx === 0) {
      setThumbnailUrl(images[images.length - 1]);
    } else {
      setThumbnailUrl(images[matchedIdx - 1]);
    }
  };

  const swipeRightHandler = () => {
    const matchedIdx = images.findIndex((url) => url === thumbnailUrl);

    if (matchedIdx + 1 < images.length) {
      setThumbnailUrl(images[matchedIdx + 1]);
    } else {
      setThumbnailUrl(images[0]);
    }
  };

  let showProductThumbnails;
  if (images) {
    showProductThumbnails = images.map((url) => (
      <div
        key={url}
        className={classes["thumbnail"]}
        onClick={() => thumbnailSelectedHandler(url)}
      >
        <img src={url} />
      </div>
    ));
  }

  return (
    <div className={classes["product-container"]}>
      <div className={classes.title}>{name}</div>

      <div className={classes["image-container"]}>
        <div className={classes["main-image"]}>
          <i
            className={`fa fa-chevron-left ${classes["arrow-left"]}`}
            onClick={swipeLeftHandler}
          ></i>
          {thumbnailUrl !== "" && <img src={thumbnailUrl} alt={name} />}
          <i
            className={`fa fa-chevron-right ${classes["arrow-right"]}`}
            onClick={swipeRightHandler}
          ></i>
        </div>

        <div className={classes["product-thumbnails"]}>
          {showProductThumbnails}
        </div>
      </div>

      <div className={classes["product-details"]}>
        <div className={classes["title-laptop"]}>{name}</div>

        <div className={classes.divider}></div>

        <div className={classes["price-container"]}>
          <span className={classes.symbol}>$</span>
          <span className={classes.number}>
            {price ? price.toFixed(2) : null}
          </span>
        </div>

        <div className={classes["quantity-container"]}>
          <div>Quantity:</div>
          <div className={classes.control}>
            <span className={classes.decrease} onClick={decreaseHandler}>
              <i className={`fa-solid fa-minus`}></i>
            </span>
            <input value={quantity} onChange={updateQuantityHandler} />
            <span className={classes.increase} onClick={increaseHandler}>
              <i className={`fa-solid fa-add`}></i>
            </span>
          </div>
        </div>

        <Button onClick={addToCart}>ADD TO CART</Button>

        <div className={classes.wishlist}>
          <i className={`fa-regular fa-heart`}></i>
          <div className={classes.text}>Add to wishlist</div>
        </div>

        <div className={classes.divider}></div>

        <div className={classes.description}>
          {description &&
            description.split("\r\n").map((item, i) => (
              <div key={i}>
                {item}
                <br />
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

export default Product;
