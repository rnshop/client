import Card from "../UI/Card";
import Input from "../UI/Input";
import Button from "../UI/Button";
import classes from "./Customer.module.css";

const Customer = () => {
  const email = {
    id: "email",
    type: "email",
    required: true,
  };

  const firstName = {
    id: "firstName",
    type: "text",
    required: true,
  };

  const lastName = {
    id: "lastName",
    type: "text",
    required: true,
  };
  return (
    <Card className={classes.space}>
      <div className={classes.header}>1. Customer info</div>
      <div className="divider"></div>

      <Input label="Email Address" input={email} classes="input-full"></Input>

      <div className="row">
        <Input
          label="First Name"
          input={firstName}
          classes="input-half"
        ></Input>

        <Input label="Last Name" input={lastName} classes="input-half"></Input>
      </div>

      <Button>Next</Button>
    </Card>
  );
};

export default Customer;
