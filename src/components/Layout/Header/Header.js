import { Fragment } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import HeaderCartButton from "./HeaderCartButton";
import SearchBar from "./SearchBar";

import logo from "../../../assets/logo.png";
import classes from "./Header.module.css";
import { uiActions } from "../../../store/ui-slice";
import UserDropdown from "./UserDropdown";

const Header = () => {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth.auth);
  const { isDropdownOpen } = useSelector((state) => state.ui);
  const isAuthenticated = auth.accessToken; // && tokenIsValid

  const userIconHandler = () => {
    if (!isAuthenticated) {
      dispatch(uiActions.openLoginModal());
    } else {
      if (!isDropdownOpen) {
        dispatch(uiActions.openDropdown());
      } else {
        dispatch(uiActions.closeDropdown());
      }
    }
  };

  const iconBlurHandler = () => {
    console.log("aaa");
  };

  return (
    <Fragment>
      <header className={classes.header}>
        <div className={classes.laptop}>
          <div className={classes.image}>
            <Link to="/">
              <img src={logo} alt="Hayes Logo" />
            </Link>
          </div>
          <div className={classes["laptop-searchBar"]}>
            <SearchBar />
          </div>
          <div className={classes.icons}>
            {/* {isAuthenticated && <div>Hi, {auth.firstName}</div>} */}
            <i
              className={`fa-regular fa-user ${classes["user-icon"]}`}
              onClick={userIconHandler}
            ></i>

            {isAuthenticated && isDropdownOpen && <UserDropdown />}
            <HeaderCartButton />
          </div>
        </div>

        <div className={classes["ipad-searchBar"]}>
          <SearchBar />
        </div>
      </header>

      {/* <div className={classes["main-image"]}>
        <img src={banner} alt="Hayes Banner" />
      </div> */}
    </Fragment>
  );
};

export default Header;
