import { useDispatch, useSelector } from "react-redux";
import { uiActions } from "../../store/ui-slice";
import CartProduct from "./CartProduct";

import classes from "./CartProductList.module.css";

const CartProductList = () => {
  const items = useSelector((state) => state.cart.items);
  const dispatch = useDispatch();

  const closeCartModal = () => {
    dispatch(uiActions.closeCartModal());
  };

  return (
    <div className={classes.products}>
      {items.length === 0 && (
        <div className={classes["empty-cart"]}>
          <div>Your cart is empty</div>
          <div className={classes["continue"]} onClick={closeCartModal}>
            Continue shopping
          </div>
        </div>
      )}
      {items.length > 0 &&
        items.map((item) => <CartProduct key={item._id} item={item} />)}
    </div>
  );
};

export default CartProductList;
