import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isCartModalOpen: false,
  isLoginModalOpen: false,
  isDropdownOpen: false
};

const uiSlice = createSlice({
  name: "ui",
  initialState,
  reducers: {
    openCartModal(state) {
      state.isCartModalOpen = true;
    },
    closeCartModal(state) {
      state.isCartModalOpen = false;
    },

    openLoginModal(state) {
      state.isLoginModalOpen = true;
    },
    closeLoginModal(state) {
      state.isLoginModalOpen = false;
    },

    openDropdown(state) {
      state.isDropdownOpen = true;
    },
    closeDropdown(state) {
      state.isDropdownOpen = false;
    },
  },
});

export const uiActions = uiSlice.actions;

export default uiSlice.reducer;
