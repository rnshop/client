import { useDispatch, useSelector } from "react-redux";
import classes from "./HeaderCartButton.module.css";
import { uiActions } from "../../../store/ui-slice";

const HeaderCartButton = (props) => {
  const dispatch = useDispatch();
  const cartItems = useSelector((state) => state.cart.items);

  const numOfCartItems = cartItems.reduce((currentNum, item) => {
    return currentNum + item.quantity;
  }, 0);

  const openCartHandler = () => {
    dispatch(uiActions.openCartModal());
  };

  return (
    <div onClick={openCartHandler} className={classes.button}>
      <span>
        <i className={`fa-solid fa-cart-shopping`}></i>
      </span>
      <span className={classes.badge}>{numOfCartItems}</span>
    </div>
  );
};

export default HeaderCartButton;
