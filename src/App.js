import React, { Suspense } from "react";
import { Switch, Route } from "react-router";

import Layout from "./components/Layout/Layout";
import PersistLogin from "./components/Auth/PersistLogin";
// import AllProducts from "./pages/AllProducts";
// import ProductDetails from "./pages/ProductDetails";
// import CartPage from "./pages/CartPage";
// import Checkout from "./pages/Checkout";

import Orders from "./components/MyOrders/Orders";
import Wishlist from "./components/MyWishlist/Wishlist";

const AllProducts = React.lazy(() => import("./pages/AllProducts"));
const ProductDetails = React.lazy(() => import("./pages/ProductDetails"));
const CartPage = React.lazy(() => import("./pages/CartPage"));
const Checkout = React.lazy(() => import("./pages/Checkout"));
const AddProduct = React.lazy(() => import("./pages/AddProduct"));

const App = () => {
  return (
    <Layout>
      <Suspense fallback={<div>Loading ...</div>}>
        <Switch>
          <PersistLogin>
            <Route path="/" exact>
              <AllProducts />
            </Route>
            <Route path="/product/:name">
              <ProductDetails />
            </Route>
            <Route path="/checkout">
              <Checkout />
            </Route>
            <Route path="/cart">
              <CartPage />
            </Route>

            <Route path="/orders">
              <Orders />
            </Route>
            <Route path="/wishlist">
              <Wishlist />
            </Route>

            <Route path="/admin/addProduct">
              <AddProduct />
            </Route>
          </PersistLogin>
        </Switch>
      </Suspense>
    </Layout>
  );
};

export default App;
