import { Fragment, useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import useInput from "../../hooks/useInput";
import { login } from "../../services/user.service";

import Input from "../UI/Input";
import Button from "../UI/Button";
import classes from "./Auth.module.css";
import { authActions } from "../../store/auth-slice";

const Login = (props) => {
  const dispatch = useDispatch();
  const emailRef = useRef();
  const [errMsg, setErrMsg] = useState("");

  const isNotEmpty = (value) => value.trim() !== "";

  const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const isValidEmail = (value) => value.match(emailRegex);

  const emailProp = {
    id: "email",
    type: "email",
    required: true,
    autoComplete: "off",
  };

  const passwordProp = {
    id: "password",
    type: "password",
    required: true,
  };

  const {
    value: email,
    isValid: emailIsValid,
    hasError: emailHasError,
    inputChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
  } = useInput(isValidEmail);

  const {
    value: password,
    isValid: passwordIsValid,
    hasError: passwordHasError,
    inputChangeHandler: passwordChangeHandler,
    inputBlurHandler: passwordBlurHandler,
  } = useInput(isNotEmpty);

  let formIsValid = false;
  if (emailIsValid && passwordIsValid) {
    formIsValid = true;
  }

  useEffect(() => {
    emailRef.current.focus();
  }, []);

  useEffect(() => {
    setErrMsg("");
  }, [email, password]);

  const submitHandler = async (e) => {
    e.preventDefault();

    if (formIsValid) {
      const userData = {
        email,
        password,
      };

      try {
        const response = await login(userData);
        // console.log("user logged in success ", response);

        const auth = {
          email,
          firstName: response?.data?.firstName,
          accessToken: response?.data?.accessToken,
        };
        dispatch(authActions.setAuth(auth));
        props.onCloseModal();

      } catch (err) {
        // console.log(err);
        setErrMsg("Please fill in valid email and password");
      }
    }
  };

  return (
    <Fragment>
      {errMsg && <div className={classes.alert}>{errMsg}</div>}
      <form onSubmit={submitHandler}>
        <Input
          ref={emailRef}
          label="Email"
          classes="input-full"
          input={emailProp}
          value={email}
          autoComplete="off"
          onChange={emailChangeHandler}
          onBlur={emailBlurHandler}
        />
        {emailHasError && (
          <p className="error-text">Please fill in a valid email.</p>
        )}

        <Input
          label="Password"
          classes="input-full"
          input={passwordProp}
          value={password}
          onChange={passwordChangeHandler}
          onBlur={passwordBlurHandler}
        />
        {passwordHasError && <p className="error-text">Password is required</p>}

        <Button disabled={!formIsValid}>Login</Button>
      </form>
    </Fragment>
  );
};

export default Login;
