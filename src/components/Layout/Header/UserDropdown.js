import { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import useLogout from "../../../hooks/useLogout";
import { uiActions } from "../../../store/ui-slice";
import classes from "./UserDropdown.module.css";

const UserDropdown = () => {
  const dispatch = useDispatch();
  const logout = useLogout();
  const history = useHistory();
  const auth = useSelector((state) => state.auth.auth);

  const selectDropdownHandler = () => {
    dispatch(uiActions.closeDropdown());
  };

  const logoutHandler = async () => {
    await logout();
    history.push("/");
  };

  return (
    <div className={classes.dropdown}>
      <div className={classes.welcome}>Welcome back, {auth.firstName}</div>
      <ul onClick={selectDropdownHandler}>
        <Link to="/orders">My Orders</Link>
        <Link to="/wishlist">My Wishlist</Link>
        <Link to="/myAccount">My Account</Link>
        <li onClick={logoutHandler}>Logout</li>
      </ul>
    </div>
  );
};

export default UserDropdown;
