import Modal from "../UI/Modal";
import classes from "./CartModal.module.css";

const CartModal = (props) => {
  return (
    <Modal classes={classes["cart-modal"]} onClose={props.onClose}>
      {props.children}
    </Modal>
  );
};

export default CartModal;
