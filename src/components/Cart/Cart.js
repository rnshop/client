import { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { uiActions } from "../../store/ui-slice";

import Button from "../UI/Button";
import CartModal from "./CartModal";
import CartProductList from "./CartProductList";

import classes from "./Cart.module.css";

const Cart = () => {
  const history = useHistory();
  const { items, totalAmount } = useSelector((state) => state.cart);
  const dispatch = useDispatch();

  const checkoutHandler = () => {
    history.push("/checkout");
    closeCartModal();
  };

  const closeCartModal = () => {
    dispatch(uiActions.closeCartModal());
  };

  return (
    <CartModal onClose={closeCartModal}>
      <div className={classes["cart-content"]}>
        <div className={classes["top-title"]}>
          <span onClick={closeCartModal}>
            <i
              className={`${classes["close-arrow"]} fa-solid fa-arrow-right`}
            ></i>
          </span>
          <div className={classes["title-text"]}>Your cart</div>

          <div className="divider"></div>
        </div>

        <CartProductList items={items} />

        <div className="divider"></div>

        {items.length > 0 && (
          <Fragment>
            <div className={classes.subtotal}>
              <span>Subtotal</span>
              <span>${totalAmount.toFixed(2)}</span>
            </div>
            <Button onClick={checkoutHandler}>Checkout</Button>
            <div
              className={classes["continue-shopping"]}
              onClick={closeCartModal}
            >
              Continue shopping
            </div>
          </Fragment>
        )}
      </div>
    </CartModal>
  );
};
export default Cart;
