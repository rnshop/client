import Card from "../UI/Card";
import Input from "../UI/Input";
import Button from "../UI/Button";
import classes from "./Delivery.module.css";

const Delivery = () => {
  const company = {
    id: "company",
    type: "text",
    required: false,
  };

  const suburb = {
    id: "suburb",
    type: "text",
    required: true,
  };

  const city = {
    id: "city",
    type: "text",
    required: true,
  };

  const state = {
    id: "state",
    type: "text",
    required: true,
  };

  const postcode = {
    id: "postcode",
    type: "text",
    required: true,
  };

  const phone = {
    id: "phone",
    type: "text",
    required: true,
  };

  return (
    <Card>
      <div className={classes.header}>2. Delivery</div>
      <div className="divider"></div>

      <Input label="Company Name" input={company} classes="input-full"></Input>

      <div className="row">
        <Input label="Suburb" input={suburb} classes="input-half"></Input>

        <Input label="City" input={city} classes="input-half"></Input>
      </div>

      <div className="row">
        <Input label="State" input={state} classes="input-half"></Input>

        <Input label="Postcode" input={postcode} classes="input-half"></Input>
      </div>

      <Input label="Phone" input={phone} classes="input-full"></Input>

      <Button>Next</Button>
    </Card>
  );
};

export default Delivery;
