import http from "../http";

export const registerUser = (userData) => {
  try {
    const response = http.post("/register", userData);
    return response;
  } catch (err) {
    return err;
  }
};


export const login = (userData) => {
  try {
    const response = http.post("/login", userData);
    return response;
  } catch (err) {
    return err;
  }
};
