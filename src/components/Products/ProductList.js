import classes from "./ProductList.module.css";
import ProductItem from "./ProductItem";

const ProductList = (props) => {
  return (
    <div className={classes["products-container"]}>
      <div className={classes.products}>
        {props.products.map((product) => (
          <ProductItem key={product._id} product={product} />
        ))}
      </div>
    </div>
  );
};

export default ProductList;
